#include <QObject>
#include <QApplication>
#include <QUrl>
#include <QWebEngineView>
#include <QtWebEngine>

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);

	// display a web page on a widget
	QtWebEngine::initialize();
	QWebEngineView view;
	view.load(QUrl("http://www.qt.io/"));
	view.resize(1024, 750);
	view.show();

	a.exec();
}

