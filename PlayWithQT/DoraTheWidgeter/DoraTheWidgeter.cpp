#include "DoraTheWidgeter.h"
#include <qfiledialog.h>

DoraTheWidgeter::DoraTheWidgeter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	QObject::connect(ui.loadImageButton,
		&QAbstractButton::clicked,
		[this]()
		{
			QString selectedFileName = QFileDialog::getOpenFileName(this, "Choose an image..", "", "*.png *.jpg *.bmp");
			ui.inputFilePathLineEdit->setText(selectedFileName);
			ui.imageLabel->setPixmap(QPixmap(selectedFileName));
		});
}
