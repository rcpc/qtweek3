#include <QtWidgets/QApplication>
#include "DoraTheWidgeter.h"

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);

	DoraTheWidgeter window;
	window.show();

	return a.exec();
}