#pragma once

class CustomType
{
public:
	CustomType(int aValue): m_value(aValue)
	{}

	int getValue() const
	{
		return m_value;
	}

private:
	int m_value{ 5 };
};