#pragma once
#include <qobject.h>

class Parent : public QObject
{
public:
	Parent() : QObject()
	{

	}

	~Parent()
	{
		qDebug() << "Parent destroyed";
	}

	void mySlot()
	{
		qDebug() << "Slot reached!";
	}

	void onPropertyChanged()
	{
		qDebug() << "Some fucking property changed!";
	}

};
