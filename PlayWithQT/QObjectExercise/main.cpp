#include <iostream>
#include <QtCore/QCoreApplication>
#include <QObject>
#include <qstring.h>
#include <qsharedpointer.h>
#include <qdebug.h>
#include "View.h"
#include "State.h"
#include "main.h"


void deleteWithSharedPtr()
{
	QSharedPointer<QObject> parent = QSharedPointer<QObject>(new QObject, &QObject::deleteLater);
	QObject::connect(parent.get(), &QObject::destroyed, []() { std::cout << "View destroyed!" << std::endl; });

	for (int i = 0; i < 20; ++i)
	{
		QSharedPointer<QObject> child = QSharedPointer<QObject>(new QObject, &QObject::deleteLater);
		QObject::connect(child.get(), &QObject::destroyed, [i]()
			{
				qDebug() << QString("State %1 destroyed!").arg(i);
			});

		child->setParent(parent.get());
	}
}
void deleteWithNormalPtr()
{
	{
		QObject* parent = new QObject;
		QObject::connect(parent, &QObject::destroyed, []() {std::cout << QString("View Destroyed\n").toStdString(); });
		for (int i = 0; i < 20; ++i)
		{
			QObject* child = new QObject;
			child->setParent(parent);
			QObject::connect(child, &QObject::destroyed, [i] { qDebug() << QString("State %1 Destroyed\n").arg(i); });
		}
		delete[] parent;
	}
}
void deleteWithNormalPtrCustomClasses()
{
	{
		View* parent = new View;
	//	View::connect(parent, &View::destroyed, []() {std::cout << QString("View Destroyed\n").toStdString(); });
		for (int i = 0; i < 20; ++i)
		{
			State* child = new State;
			child->setParent(parent);
		//	State::connect(child, &State::destroyed, [i] { qDebug() << QString("State %1 Destroyed\n").arg(i); });
		}
		delete[] parent;
	}
	{
		View* parent = new View;
			State* child = new State;
			child->setParent(parent);
			delete parent;

	}
}

void slotExample()
{
	View* parent = new View;
	State* child = new State;
	
	// QObject::connect(child, qOverload<int>(&State::propertyChanged), parent, qOverload<int>(&View::onPropertyChanged));
	QObject::connect(child, &State::customPropertyChanged, parent, &View::onCustomPropertyChanged);
	child->performCustomAction();

	delete[] parent;
	delete[] child;
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	//deleteWithSharedPtr();
	//deleteWithNormalPtr();
	//deleteWithNormalPtrCustomClasses();
	slotExample();

	return a.exec();
}
