#pragma once
#include <qobject.h>
#include <qstring.h>
#include "CustomType.h"

class View : public QObject
{
public:
	View() : QObject()
	{

	}

	~View()
	{
		qDebug() << "View destroyed";
	}

	void mySlot()
	{
		qDebug() << "Slot reached!";
	}

	void onPropertyChanged(int aValue)
	{
		qDebug() << "Int reached with value: " << aValue;
	}
	void onCustomPropertyChanged(CustomType& customType)
	{
		qDebug() << "String reached with value: " << customType.getValue();
	}
	void onPropertyChanged(const QString & aValue)
	{
		qDebug() << "String reached with value: " << aValue;
	}

};
