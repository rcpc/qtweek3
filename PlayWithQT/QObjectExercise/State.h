#pragma once
#include "CustomType.h"
#include <qobject.h>
#include <qdebug.h>

class State : public QObject
{
public:

private:
	Q_OBJECT

public:
	State() : QObject()
	{
		this->performAction();
	}
	~State()
	{
		qDebug() << "State destroyed";
	}

	void performAction()
	{
		/* Stuff */
		propertyChanged(++m_myIntValue);
	}
	void performCustomAction()
	{
		/* Stuff */
		customPropertyChanged(m_customType);
	}


private:
	CustomType m_customType{ 3 };
	int m_myIntValue = 0;

signals:
	void mySignal();

	void propertyChanged(int) const;
	void propertyChanged(const QString&) const;
	void customPropertyChanged(CustomType&) const;
};
