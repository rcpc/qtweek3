#pragma once
#pragma once
#include <qobject.h>
#include <qdebug.h>

class Child : public QObject
{
private:
	Q_OBJECT

public:
	Child() : QObject()
	{

	}

	~Child()
	{
		qDebug() << "Child destroyed";
	}

	void performAction()
	{
		/* Stuff */
		mySignal();
	}

signals:
	void mySignal();
	void propertyChanged();

};
